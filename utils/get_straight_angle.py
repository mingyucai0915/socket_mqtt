#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import sys
import signal
import argparse
import paho.mqtt.client as mqtt
import json
import time
import numpy as np
import os
from utils.mathutils import quaternion_to_euler_angle
from scipy import optimize
import matplotlib.pyplot as plt
import logging

idx = 0
POINTS = 5000
points_array = np.ndarray(shape=(2, POINTS), dtype=np.float)
quat_array = np.ndarray(shape=(4, POINTS), dtype=np.float)
points_array.fill(0)
quat_array.fill(0)
s_sum = np.zeros((1, 3), dtype=np.float)
THRESHOLD = 0.5  # m

try_time = 5
diff_file = "../save_variable/diff_1.npz"
data_file = "../save_variable/straight_test.npz"

finished = True

logging.basicConfig(format='%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
    datefmt='%d-%m-%Y:%H:%M:%S',
    level=logging.INFO)
logger = logging.getLogger(__name__)


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe("socket_mqtt")


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    global idx, points_array, finished, quat_array, diff
    if finished:
        return
    try:
        data = json.loads(msg.payload)
    except ValueError as e:
        # logger.error("Value error. Message is: {}".format(msg.payload))
        return

    # print("Time: {}.".format(cur_time))
    x = float(data["x"])
    y = float(data["y"])
    z = float(data["z"])

    qw = float(data["qw"])
    qx = float(data["qx"])
    qy = float(data["qy"])
    qz = float(data["qz"])

    points_array[:, idx] = [x, y]
    quat_array[:, idx] = [qw, qx, qy, qz]

    if idx == 0:  # 0 point
        idx += 1
        return

    if np.linalg.norm(points_array[:, idx] - points_array[:, 0]) > THRESHOLD:  # terminating condition
        points_array = points_array[:, 0:idx + 1]
        quat_array = quat_array[:, 0:idx + 1]
        # plot path
        plt.figure("Path")
        plt.scatter(points_array[0, :],
                    points_array[1, :])
        plt.xlim(0.5, 4)
        plt.ylim(-3, 0)
        plt.show()

        k, b = np.polyfit(points_array[0, :], points_array[1, :], 1)

        v_x = 1 if points_array[0, -1] > points_array[0, 0] else -1
        v_y = v_x * k
        robot_front = np.arctan2(v_y, v_x)
        quat_value = np.mean(quat_array, axis=1)
        eulers = quaternion_to_euler_angle(quat_value[0], quat_value[1], quat_value[2], quat_value[3])

        diff = (robot_front - eulers[2, 0])
        if diff >= np.pi:
            diff = diff - 2 * np.pi
        elif diff <= - np.pi:
            diff = diff + 2 * np.pi
        print("Quat: [{}, {}, {}, {}]".format(quat_value[0], quat_value[1], quat_value[2], quat_value[3]))
        print("Euler angles: [{}, {}, {}]".format(eulers[0], eulers[1], eulers[2]))
        print("k is {}, b is: {}".format(k, b))
        print("e[z] = {}rad, robot_front = {}rad".format(eulers[2], robot_front))
        print("diff between robot front and euler_z: {} rad.".format(diff))

        np.savez(os.path.abspath(data_file), robot_front=robot_front, points_array=points_array, quat_array=quat_array)
        finished = True
        return

    if idx % 100 == 0:
        print("idx: {}".format(idx))
    idx += 1


if __name__ == "__main__":
    def signal_handler(sig, frame):
        print('Exiting...')
        client.disconnect()
        sys.exit(0)


    signal.signal(signal.SIGINT, signal_handler)

    parser = argparse.ArgumentParser(description="Robot circle finder. ")
    parser.add_argument("-v", "--verbose", default=False, help="Verbose output. ", action="store_true")

    args = parser.parse_args()
    verbose = args.verbose

    # MQTT
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.enable_logger(logger=logger)
    client.connect("localhost", 1883, 60)

    # circle starts
    client.publish("robot1", json.dumps({"l": 100, "r": 100}))
    client.loop_start()

    i = 0
    diff_list = []
    while try_time > 0:
        print("Start get {} line angle diff.".format(try_time))
        try_time -= 1
        finished = False
        while not finished:
            pass
        diff_list.append(diff)
        print("{} line angle diff: {} rad.".format(try_time, diff))
        # clear
        idx = 0
        points_array = np.ndarray(shape=(2, POINTS), dtype=np.float)
        quat_array = np.ndarray(shape=(4, POINTS), dtype=np.float)
        points_array.fill(0)
        quat_array.fill(0)

        print("Sleep 5 seconds.")
        time.sleep(5)
    print(diff_list)
    print("Diff avg: {}".format(np.mean(np.array(diff_list))))
    np.savez(os.path.abspath(diff_file), diff=np.mean(np.array(diff_list)))
    client.loop_stop()
    exit(0)
