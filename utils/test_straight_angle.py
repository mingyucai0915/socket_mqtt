#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import sys
import signal
import argparse
import paho.mqtt.client as mqtt
import json
import time
import random
import numpy as np

idx = 0
POINTS = 5000
p_c = np.ndarray(shape=(2, POINTS), dtype=np.float)
qz_array = np.ndarray(shape=(1, POINTS), dtype=np.float)
p_c.fill(0)
THRESHOLD = 1
diff_file = "../save_variable/diff_1.npz"
data_file = "../save_variable/straight_test.npz"

npzfile = np.load(diff_file)
diff = npzfile["diff"]


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe("socket_mqtt")


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    global idx, last_time, sum, qz_array, p_c
    data = json.loads(msg.payload)
    # print("Time: {}.".format(cur_time))
    x = float(data["x"])
    y = float(data["y"])
    z = float(data["z"])

    qx = float(data["qx"])
    qy = float(data["qy"])
    qz = float(data["qz"])

    p_c[:, idx] = [x, y]
    qz_array[0, idx] = qz

    if idx == 0:  # initial point
        idx += 1
        return

    if np.linalg.norm(p_c[:, idx] - p_c[:, 0]) > THRESHOLD or idx >= 5000:  # terminating condition
        if idx == 5000:
            last_one = 4999
        else:
            last_one = np.max(np.where(p_c[0, :] > 0))

        p_c = p_c[:, 0:last_one+1]
        qz_array = qz_array[0, 0:last_one]
        print("pc[:, 0]: [{}, {}]; qz_array[0]: [{}]".format(p_c[0, 0], p_c[1, 0], qz_array[0]))
        print("pc[:, -1]: [{}, {}]; qz_array[-1]: [{}]".format(p_c[0, -1], p_c[1, -1], qz_array[-1]))

        k, b = np.polyfit(p_c[0, :], p_c[1, :], 1)

        theta = np.arctan(k)
        v_x = 1 if p_c[0, -1] > p_c[0, 0] else -1
        v_y = v_x * k
        angle_with_x = np.arctan2(v_y, v_x)

        q_z = np.mean(qz_array[:])

        print("k is {}, b is: {}".format(k, b))
        print("Angle between robot front and x is: {} rad.".format(theta))
        print("qz from motive measurement is {}".format(q_z))
        print("qz from calculation is {}".format(angle_with_x - diff))


        exit(0)
        pass
    else:  # get new data
        p_c[:, idx] = [x, y]
        idx += 1

    sum += np.array([x, y, z], dtype=np.float)

    if idx > 5000:
        sum /= 5000
        print("[{}, {}, {}]".format(sum[0], sum[1], sum[2]))
        exit(0)
    if idx % 100 == 0:
        print("idx: {}".format(idx))
    idx += 1


if __name__ == "__main__":
    def signal_handler(sig, frame):
        print('Exiting...')
        set_speed(client, 0, 0)
        client.disconnect()
        sys.exit(0)


    signal.signal(signal.SIGINT, signal_handler)

    parser = argparse.ArgumentParser(description="Robot control algorithm. ")
    parser.add_argument("-v", "--verbose", default=False, help="Verbose output. ", action="store_true")

    args = parser.parse_args()
    verbose = args.verbose

    # MQTT
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message

    client.connect("localhost", 1883, 60)
    client.loop_forever()
