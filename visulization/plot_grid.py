import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.collections import PatchCollection
import matplotlib.image as image


robot_location = [0.5, 0.5]
robot_theta = 0
predict_path = [0, 1, 2, 3, 7]  # 0-31
target_location = [1.5, 3.5]

# background figure
fig_bg = plt.figure(num="Background", figsize=(8, 4))
ax = fig_bg.add_subplot(111)
ax.set_xlim(0, 8)
ax.set_ylim(0, 4)
ax.set_xticks(np.arange(9))
ax.set_yticks(np.arange(5))
ax.grid()


# draw robot
im = image.imread("turtle.png")
width, height = (0.5, 0.5)
ax.imshow(im, aspect='auto', extent=(robot_location[0] - width/2, robot_location[0] + width/2,
                                     robot_location[1] - height / 2, robot_location[1] + height / 2), zorder=99)

# draw predicted path
rects = []
for path in predict_path:
    rect = patches.Rectangle(xy = (path//4, path%4), width=1, height=1)
    rects.append(rect)

collection = PatchCollection(rects, alpha=0.5)
ax.add_collection(collection)

# draw target location
flag = image.imread("flag.png")
ax.imshow(flag, aspect='auto', extent=(target_location[0] - width/2, target_location[0] + width/2,
                                     target_location[1] - height / 2, target_location[1] + height / 2), zorder=99)

plt.show()
